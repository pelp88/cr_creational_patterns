package lab3;

import lab3.builder.GlassBuilder;
import lab3.director.Director;

public class Launcher {
    public static void main(String[] args) {

        GlassBuilder glassBuilder = new GlassBuilder();
        Director.buildCrystalGlass(glassBuilder);
        System.out.println(glassBuilder.create());

        Director.buildGlassMadeOfGlass(glassBuilder);
        System.out.println(glassBuilder.create());

        Director.buildPlasticGlass(glassBuilder);
        System.out.println(glassBuilder.create());
    }
}
