package lab3.builder;

import lab3.components.Manufacturer;
import lab3.components.Material;
import lab3.glass.Glass;

public class GlassBuilder {
    private Manufacturer manufacturer;
    private Material material;
    private double manufacturingCost;

    public void setManufacturer(Manufacturer manufacturer){
        this.manufacturer = manufacturer;
    }

    public void setMaterial(Material material){
        this.material = material;
    }

    public void setManufacturingCost(double cost){
        this.manufacturingCost = cost;
    }

    public Glass create(){
        return new Glass(this.manufacturer,
                this.material,
                this.manufacturingCost);
    }
}
