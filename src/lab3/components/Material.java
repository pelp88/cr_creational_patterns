package lab3.components;

public abstract class Material {
    public String type = this.getClass().getSimpleName();
    public double cost;

    public Material(double cost){
        this.cost = cost;
    }

    public String getType(){
        return this.type;
    }

    public double getCost(){
        return this.cost;
    }
}
