package lab3.components;

public class Manufacturer {
    private String name;
    private String telephoneNumber;

    public Manufacturer(String name, String telephoneNumber){
        this.name = name;
        this.telephoneNumber = telephoneNumber;
    }

    public String getName(){
        return this.name;
    }

    public String getTelephoneNumber(){
        return this.telephoneNumber;
    }
}
