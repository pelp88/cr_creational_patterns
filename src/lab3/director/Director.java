package lab3.director;

import lab3.builder.GlassBuilder;
import lab3.components.CrystalMaterial;
import lab3.components.GlassMaterial;
import lab3.components.Manufacturer;

public class Director {
    public static void buildCrystalGlass(GlassBuilder builder){
        builder.setManufacturer(
                new Manufacturer("Glass Inc.", "000"));
        builder.setMaterial(new CrystalMaterial(100));
        builder.setManufacturingCost(10);
    }

    public static void buildGlassMadeOfGlass(GlassBuilder builder){
        builder.setManufacturer(
                new Manufacturer("Russian Glass International", "111"));
        builder.setMaterial(new GlassMaterial(50));
        builder.setManufacturingCost(5);
    }

    public static void buildPlasticGlass(GlassBuilder builder){
        builder.setManufacturer(
                new Manufacturer("Plastic Recycling", "222"));
        builder.setMaterial(new CrystalMaterial(5));
        builder.setManufacturingCost(1);
    }
}
