package lab3.glass;

import lab3.components.Manufacturer;
import lab3.components.Material;

public class Glass {
    private final Manufacturer manufacturer;
    private final Material material;
    private final double manufacturingCost;

    public Glass(Manufacturer manufacturer, Material material, double manufacturingCost) {
        this.manufacturer = manufacturer;
        this.material = material;
        this.manufacturingCost = manufacturingCost;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public Material getMaterial() {
        return material;
    }

    public double getManufacturingCost() {
        return manufacturingCost;
    }

    @Override
    public String toString(){
        return "Manufacturer: " + manufacturer.getName()
                + " " + manufacturer.getTelephoneNumber()
                + "\nMaterial: " + material.getType()
                + "\nFinal cost: " + manufacturingCost +
                " + " + material.getCost() + " = "
                + (manufacturingCost + material.getCost());
    }
}
