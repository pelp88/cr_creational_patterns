package lab8.classes;

public enum Colors {
    BLACK("Полоса черного цвета"),
    WHITE("Полоса белого цвета"),
    BLUE("Полоса синего цвета"),
    RED("Полоса красного цвета"),
    YELLOW("Полоса желтого цвета");

    private String strip;

    private Colors(String strip){
        this.strip = strip;
    }

    @Override
    public String toString(){
        return this.strip;
    }
}
