package lab8.classes;

public class GermanFlag extends Flag{
    public GermanFlag() {
        super("Германия");
    }

    @Override
    public void drawFlag() {
        drawCountry();
        System.out.println(Colors.BLACK);
        System.out.println(Colors.RED);
        System.out.println(Colors.YELLOW);
    }
}
