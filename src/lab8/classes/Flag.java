package lab8.classes;

public abstract class Flag {
    protected String country;

    public Flag(String country){
        this.country = country;
    }

    public abstract void drawFlag();

    protected void drawCountry(){
        System.out.println("Флаг: " + this.country);
    }
}
