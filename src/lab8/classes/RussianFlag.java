package lab8.classes;

public class RussianFlag extends Flag {
    public RussianFlag() {
        super("Россия");
    }

    @Override
    public void drawFlag() {
        drawCountry();
        System.out.println(Colors.WHITE);
        System.out.println(Colors.BLUE);
        System.out.println(Colors.RED);
    }
}
