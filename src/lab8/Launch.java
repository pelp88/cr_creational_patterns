package lab8;

import lab8.classes.GermanFlag;
import lab8.classes.RussianFlag;

public class Launch {
    public static void main(String[] args) {
        var russia = new RussianFlag();
        russia.drawFlag();

        var germany = new GermanFlag();
        germany.drawFlag();
    }
}
