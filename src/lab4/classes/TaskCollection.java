package lab4.classes;

public interface TaskCollection {
    public TaskListIterator getIterator();
}
