package lab4.classes;

import java.util.List;

public class Task implements TaskCollection {
    String day;
    List<String> tasks;

    public Task(String day, List<String> tasks) {
        this.day = day;
        this.tasks = tasks;
    }

    @Override
    public TaskListIterator getIterator() {
        return new TaskIterator();
    }

    private class TaskIterator implements TaskListIterator {
        int index;

        @Override
        public boolean hasNext() {
            return index < tasks.size();
        }

        @Override
        public Object getNext() {
            return tasks.get(index++);
        }
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<String> getTask() {
        return tasks;
    }

    public void setTask(List<String> tasks) {
        this.tasks = tasks;
    }
}
