package lab4.classes;

public interface TaskListIterator {
    public boolean hasNext();
    public Object getNext();
}
