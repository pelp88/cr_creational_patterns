package lab4;

import lab4.classes.Task;

import java.util.List;

public class Launch {
    public static void main(String[] args) {
        var taskList = List.of("Task1", "Task2", "Task3");
        var tasks = new Task("Monday", taskList);
        var iterator = tasks.getIterator();

        System.out.println(tasks.getDay());

        System.out.println("Tasks:");
        while (iterator.hasNext()) {
            System.out.println(iterator.getNext().toString());
        }

    }
}
