package lab7.classes;

public class AdultButterflyState extends State {
    public AdultButterflyState(Butterfly butterfly) {
        super(butterfly);
    }

    @Override
    public void grow() {
        System.out.println(this.butterfly + " уже выросла");
    }


    @Override
    public void isCaterpillar() {
        System.out.println(this.butterfly + " уже бабочка!");
    }

    @Override
    public void isCocoon() {
        System.out.println(this.butterfly + " уже бабочка!");
    }

    @Override
    public void isAdultButterfly() {
        System.out.println(this.butterfly + " летает");
    }
}
