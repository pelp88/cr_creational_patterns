package lab7.classes;

public class Butterfly {
    private String name;
    private State state = new CaterpillarState(this);

    public Butterfly(String name){
        this.name = name;
    }

    public void changeState(State state){
        this.state = state;
    }

    public void grow(){
        state.isCaterpillar();
        state.isCocoon();
        state.isAdultButterfly();
        state.grow();
    }

    @Override
    public String toString(){
        return this.name;
    }
}
