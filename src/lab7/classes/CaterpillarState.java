package lab7.classes;

public class CaterpillarState extends State {
    public CaterpillarState(Butterfly butterfly) {
        super(butterfly);
    }

    @Override
    public void grow() {
        butterfly.changeState(new CocoonState(butterfly));
    }


    @Override
    public void isCaterpillar() {
        System.out.println(this.butterfly + " ползёт");
    }

    @Override
    public void isCocoon() {
        System.out.println(this.butterfly + " еще гусеница!");
    }

    @Override
    public void isAdultButterfly() {
        System.out.println(this.butterfly + " еще гусеница!");
    }
}
