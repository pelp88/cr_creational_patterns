package lab7.classes;

public abstract class State {
    protected Butterfly butterfly;

    public State(Butterfly butterfly){
        this.butterfly = butterfly;
    }

    public abstract void grow();
    public abstract void isCaterpillar();
    public abstract void isCocoon();
    public abstract void isAdultButterfly();
}
