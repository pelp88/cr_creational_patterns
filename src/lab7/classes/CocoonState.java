package lab7.classes;

public class CocoonState extends State {
    public CocoonState(Butterfly butterfly) {
        super(butterfly);
    }

    @Override
    public void grow() {
        butterfly.changeState(new AdultButterflyState(butterfly));
    }


    @Override
    public void isCaterpillar() {
        System.out.println(this.butterfly + " уже в коконе!");
    }

    @Override
    public void isCocoon() {
        System.out.println(this.butterfly + " спит");
    }

    @Override
    public void isAdultButterfly() {
        System.out.println(this.butterfly + " еще в коконе!");
    }
}
