package lab7;

import lab7.classes.Butterfly;

public class Launch {
    public static void main(String[] args) {
        var butterfly = new Butterfly("Test");

        butterfly.grow();
        butterfly.grow();
        butterfly.grow();
        butterfly.grow();
    }
}
