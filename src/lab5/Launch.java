package lab5;

import lab5.classes.Publisher;
import lab5.classes.Subscriber;

public class Launch {
    public static void main(String[] args) {
        var publisher = new Publisher();

        new Subscriber("Test1", publisher);
        new Subscriber("Test2", publisher);

        publisher.notifySubscribers("Test message");
    }
}
