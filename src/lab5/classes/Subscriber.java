package lab5.classes;

public class Subscriber {
    private String name;

    public Subscriber(String name, Publisher publisher){
        this.name = name;
        publisher.registerSubscriber(this);
    }

    public void notify(String message){
        System.out.println(this.name + " notified about: " + message);
    }
}
