package lab5.classes;

import java.util.ArrayList;
import java.util.List;

public class Publisher {
    private final List<Subscriber> subscribers;

    public Publisher(){
        this.subscribers = new ArrayList<>();
    }

    public void registerSubscriber(Subscriber subscriber){
        this.subscribers.add(subscriber);
    }

    public void unregisterSubscriber(Subscriber subscriber){
        this.subscribers.remove(subscriber);
    }

    public void notifySubscribers(String message){
        for (var subscriber : this.subscribers){
            subscriber.notify(message);
        }
    }
}
