package lab1;

import lab1.classes.CoffeeFactory;
import lab1.classes.CoffeeType;

public class Launch {
    public static void main(String[] args) {
        for (var type : CoffeeType.values()) {
            var coffee = CoffeeFactory.makeCoffee(type);
            coffee.grindCoffee();
            coffee.makeCoffee();
            coffee.pourIntoCup();
            System.out.println(coffee + " coffee is ready!");
        }
    }
}
