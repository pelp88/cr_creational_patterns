package lab1.classes;

public abstract class Coffee {
    private final String coffeeName = this.getClass().getSimpleName();

    public void grindCoffee(){
        System.out.println(this.coffeeName + " coffee beans are grinded");
    }

    public void makeCoffee(){
        System.out.println(this.coffeeName + " coffee brewed");
    }

    public void pourIntoCup(){
        System.out.println(this.coffeeName + " coffee poured into cup");
    }

    @Override
    public String toString(){
        return this.coffeeName;
    }
}
