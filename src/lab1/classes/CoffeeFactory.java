package lab1.classes;

public class CoffeeFactory {
    public static Coffee makeCoffee(CoffeeType coffeeType) {
        switch (coffeeType) {
            case AMERICANO:
                return new Americano();
            case LATTE:
                return new Latte();
            case ROUGH:
                return new Rough();
            case ESPRESSO:
                return new Espresso();
            case CAPPUCCINO:
                return new Cappuccino();
        }

        return null;
    }
}
