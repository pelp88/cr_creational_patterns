package lab1.classes;

public enum CoffeeType {
    AMERICANO,
    CAPPUCCINO,
    ESPRESSO,
    LATTE,
    ROUGH
}
