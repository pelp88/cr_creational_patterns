package lab2.classes;

public interface Shape {
    public Quadrilateral clone();
}
