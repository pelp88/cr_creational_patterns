package lab2.classes;


public class Quadrilateral implements Shape {
    double width;
    double height;
    double angle;

    public Quadrilateral(double width, double height, double angle) {
        this.width = width;
        this.height = height;
        this.angle = getAngle(angle);
    }

    public Quadrilateral(Quadrilateral target) {
        if (target != null) {
            this.width = target.width;
            this.height = target.height;
            this.angle = getAngle(target.angle);
        }
    }

    double getAngle(double value) {
        return value;
    }

    @Override
    public Quadrilateral clone() {
        return new Quadrilateral(this);
    }

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof Quadrilateral)) {
            return false;
        } else {
            var second = (Quadrilateral) object2;
            return second.width == width && second.height == height && second.angle == angle;
        }
    }
}
