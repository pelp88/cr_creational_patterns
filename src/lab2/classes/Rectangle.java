package lab2.classes;

public class Rectangle extends Quadrilateral implements Shape {
    public Rectangle(double width, double height) {
        super(width, height, 90);
    }

    public Rectangle(Rectangle target) {
        super(target);
    }

    @Override
    public Rectangle clone() {
        return new Rectangle(this);
    }
}
