package lab2.classes;


public class Rhombus extends Quadrilateral implements Shape {
    public Rhombus(double width, double angle) {
        super(width, width, angle);
    }

    public Rhombus(Rhombus target) {
        super(target);
    }

    @Override
    public Rhombus clone() {
        return new Rhombus(this);
    }
}
