package lab2.classes;

/**
 * Квадрат - это частный случай прямоугольника с равными сторонами
 */
public class Square extends Quadrilateral implements Shape {
    public Square(int width) {
        super(width, width, 90);
    }

    public Square(Square target) {
        super(target);
    }

    @Override
    public Square clone() {
        return new Square(this);
    }
}
