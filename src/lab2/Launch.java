package lab2;

import lab2.classes.Rectangle;
import lab2.classes.Square;

public class Launch {
    public static void main(String[] args) {
        var figure = new Rectangle(105.4, 46.34);
        var square = new Square(15);
        var clonedRect = figure.clone();
        System.out.println(figure.equals(clonedRect));
        System.out.println(square.equals(clonedRect));
    }
}
