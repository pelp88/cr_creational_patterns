package lab9;

import lab9.classes.*;

public class Launch {
    public static void main(String[] args) {
        new CardPurchase().pay();
        new CashPaperPurchase().pay();
        new CashCoinsPurchase().pay();
        new TransferPurchase().pay();
    }
}
