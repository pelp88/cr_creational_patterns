package lab9.classes;

public class CashCoinsPaymentStrategy implements PaymentStrategy{
    @Override
    public void pay() {
        System.out.println("Оплата наличными (монетами)");
    }
}
