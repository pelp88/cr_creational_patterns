package lab9.classes;

public abstract class Purchase {
    protected PaymentStrategy strategy;

    public void pay(){
        strategy.pay();
    }
}
