package lab9.classes;

public class CashPaperPurchase extends Purchase{
    public CashPaperPurchase(){
        this.strategy = new CashPaperPaymentStrategy();
    }
}
