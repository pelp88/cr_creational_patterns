package lab9.classes;

public interface PaymentStrategy {
    public void pay();
}
