package lab9.classes;

public class CashPaperPaymentStrategy implements PaymentStrategy{
    @Override
    public void pay() {
        System.out.println("Оплата наличными (банкноты)");
    }
}
