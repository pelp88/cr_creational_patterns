package lab6;

import lab6.classes.CargoPlane;
import lab6.classes.CommercialPlane;
import lab6.classes.DispatcherMediator;
import lab6.classes.LightPlane;

public class Launch {
    public static void main(String[] args) {
        DispatcherMediator mediator = new DispatcherMediator();

        var light = new LightPlane(mediator, "C1000");
        var commercial = new CommercialPlane(mediator, "12345");
        var cargo = new CargoPlane(mediator, "QWERT");

        mediator.setLightPlane(light);
        mediator.setCargoPlane(cargo);
        mediator.setCommercialPlane(commercial);

        commercial.sendAboutTakeOff();
        light.sendAboutLanding();
        cargo.sendAboutTakeOff();
        commercial.sendAboutLanding();
    }
}
