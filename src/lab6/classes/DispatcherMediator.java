package lab6.classes;

public class DispatcherMediator {
    private LightPlane lightPlane;
    private CommercialPlane commercialPlane;
    private CargoPlane cargoPlane;

    public void setLightPlane(LightPlane lightPlane) {
        this.lightPlane = lightPlane;
    }

    public void setCommercialPlane(CommercialPlane commercialPlane) {
        this.commercialPlane = commercialPlane;
    }

    public void setCargoPlane(CargoPlane cargoPlane) {
        this.cargoPlane = cargoPlane;
    }

    public void sendMessage(String message, Plane sender){
        if (!sender.equals(lightPlane)){
            lightPlane.receiveMessage(message);
        }

        if (!sender.equals(commercialPlane)){
            commercialPlane.receiveMessage(message);
        }

        if (!sender.equals(cargoPlane)){
            cargoPlane.receiveMessage(message);
        }
    }
}
