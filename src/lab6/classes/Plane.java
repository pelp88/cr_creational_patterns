package lab6.classes;

public abstract class Plane {
    private final String id;
    private final DispatcherMediator mediator;

    public Plane(DispatcherMediator mediator, String id){
        this.id = id;
        this.mediator = mediator;
    }

    public void sendAboutLanding(){
        mediator.sendMessage(id + " is landing", this);
    }

    public void sendAboutTakeOff(){
        mediator.sendMessage(id + " is taking off", this);
    }

    public void receiveMessage(String message){
        System.out.println(id + " received a message: " + message);
    }
}
